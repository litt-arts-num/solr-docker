# docker-compose `solr`

## installation
```
(sudo) git clone https://gitlab.com/litt-arts-num/solr-docker.git
cd solr-docker
(sudo) docker-compose up --build -d
```

## misc.
* le port `8983` de la machine hôte est mappé avec le port `8983` du container
* le dossier `.data` de la machine hôte est mappé avec `/var/solr` du container
* fixé à la `8.8.2` pour le moment. En version `slim`, à voir si ça pose souci
(voir les tags [ici](https://hub.docker.com/_/solr) )
